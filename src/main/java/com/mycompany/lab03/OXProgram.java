/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab03;

/**
 *
 * @author ADMIN
 */
class OXProgram {

    static boolean checkWin(String[][] table, String currentPlayer) {    
        
        if(checkRow(table,currentPlayer,0)) return true;
        if(checkRow(table,currentPlayer,1)) return true;
        if(checkRow(table,currentPlayer,2)) return true;
        
        if(checkCol(table,currentPlayer,0)) return true;
        if(checkCol(table,currentPlayer,1)) return true;
        if(checkCol(table,currentPlayer,2)) return true;
        
        if(checkDiagonalLetf(table,currentPlayer)) return true;
        if(checkDiagonalRight(table,currentPlayer)) return true;
     
        if(checkDraw(table,0)) return false;
        
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }
    
    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    
    }

    private static boolean checkDiagonalLetf(String[][] table, String currentPlayer) {
        return table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer);
    }

    private static boolean checkDiagonalRight(String[][] table, String currentPlayer) {
        return table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer);
    }
    
    private static boolean checkDraw(String[][] table,int row) {
        return table[row][0].equals("-") || table[row][1].equals("-") || table[row][2].equals("-");
    }

     
    
}
